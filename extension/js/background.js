connect();

function signIn(){
  console.log("Signing in...");
    chrome.storage.sync.get(['username', 'password'], function(items) {
      console.log('Settings retrieved', items);
      var username = items["username"];
      var password = items["password"];
      if(username && password){
        $("#uid").val(username);
        $("input[type=password]").val(password);
        $('input[type=submit]').click();
      }
    });
}

function connect(){
  console.log("Connecting...");
  if(window.location.href = "http://pre-netlogin.kuleuven.be/")
    window.location.href = "https://netlogin.kuleuven.be/cgi-bin/wayf2.pl?inst=kuleuven&lang=nl&submit=Ga+verder+%2F+Continue";
  signIn();
}