function saveCredentials(){
    console.log("Saving credentials.");
    var username = $("#username").val();
    console.log("username " + username);
    var password = $("#password").val();
    console.log("password " + password);

    chrome.storage.sync.set({'username': username, 'password': password}, function() {
      console.log('Settings saved');
    });

    $("#message").html("Settings saved.");

    signIn();
}

function getCredentials(){
    console.log("Getting credentials.");
    chrome.storage.sync.get(['username', 'password'], function(items) {
        $("#username").val(items["username"]);
        $("#password").val(items["password"]);
    });
}

function remove(){
    console.log("Removing credentials.");
    chrome.storage.sync.remove(['username', 'password']);
    $("#message").html("Settings removed.");
}

document.addEventListener('DOMContentLoaded', getCredentials);

document.getElementById("saveCredentials").addEventListener('click', saveCredentials);

document.getElementById("remove").addEventListener('click', remove);