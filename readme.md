# KotNet Chrome Extension

## How to install

Open Google Chrome. Go to Settings > Extensions > Developer Mode (top-righthand corner) and activate. Import the cloned folder "extension" with "Load unpacked extension". You're set.

## How to use
Add the extension to your tabs. Enter your r-number and password and click "Save credentials". Whenever you visit the pre-netlogin site, you will be automatically signed in.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

* [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.txt)
* [License](license.txt)